---
layout: job_family_page
title: "Distinguished Engineer, Infrastructure"
---

# Distinguished Engineer Infrastructure

The Distinguished Engineer, Infrastructure is a [grade 10](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

Specialty defined in [the Distinguished Engineer role](../../distinguished-engineer#infrastructure).
