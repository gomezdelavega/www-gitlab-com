---
layout: handbook-page-toc
title: Enterprise Risk Management (ERM)
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Purpose

The purpose of the ERM program at GitLab is to identify, assess and develop action plans for the top enterprise-wide risks that could impact the company’s ability to achieve our strategies and objectives. ERM will also help us to build a more risk aware culture by coordinating our efforts with [Security Operational Risk Management Program](https://about.gitlab.com/handbook/engineering/security/security-assurance/risk-field-security/operational-risk-management-methodology.html).

## Scope

Our ERM program is authorized by the [Audit Committee](https://about.gitlab.com/handbook/board-meetings/committees/audit/) of the Board of Directors and GitLab senior management and will be conducted using key elements of [COSO Enterprise Risk Management](https://www.coso.org/Pages/erm-integratedframework.aspx) – Integrating with Strategy and Performance.
 
GitLab’s ERM program is designed to assess operational, strategic, financial and compliance risks that could impact GitLab’s operations, financial results and/or reputation. All processes and systems implemented by GitLab to achieve its strategic objectives are in scope for ERM. 

Enterprise risks will be evaluated using three common risk assessment criteria:
- Likelihood, or the probability that a risk event may materialize; 
- Impact, or the negative financial, operational or compliance effects of a risk event; and 
- Velocity, or the speed at which a risk event were to impact GitLab. 

 
## Roles and Responsibilities

A governance structure has been established to outline the overall roles and responsibilities for ERM at GitLab.

| Role | Responsibility|
| :------ | :------ |
| Audit Committee Chairperson | Provides Board of Directors oversight of the ERM program.|
| Senior Leadership Team |Sets the “risk awareness” tone for GitLab and approves the risk appetite, risk assessment criteria and risk treatment plans for identified key risks. Drives direct reports in their respective business units to comply with the ERM program.|
|ERM Steering Committee| Provides oversight and recommendations on risk appetite, risk criteria, risk treatment and overall coordination of material risk assessment activities at GitLab.<br> <br>  The committee will be comprised of:<br><br> &#8594; Chief Financial Officer <br> &#8594; Principal Accounting Officer<br> &#8594; Senior Internal Audit Manager <br> &#8594; Chief Legal Officer <br> &#8594; Director of Legal Corporate <br> &#8594; Chief Technology Officer <br> &#8594; Vice President of Security <br> &#8594; Director, Security Risk & Compliance|
|Principal Accounting Officer|Executive Sponsor and provides oversight of Internal Audit’s execution of the ERM program and works with the senior leadership team to ensure efficient and timely execution of the program|
|Senior Internal Audit Manager| Responsible for coordinating and executing the ERM program and working with other GitLab risk assessment functions to ensure alignment on risk appetite, risk criteria and risk treatment|
|Risk Owners| Makes decisions for their specific organizations and provides insight into the day-to-day operational procedures executed by their organization in support of Risk Treatment planning. Responsible for driving risk acceptance and/or implementing remediation activities for the risks identified|

## Communication of Risks, updates to the Risk Assessment and Risk Treatment Plans


Internal Audit will communicate the top risks to management and the audit committee. Internal Audit will update the risk assessment on an annual basis or more frequently as the needs of the business changes. At the conclusion of the annual risk assessment, Risk Treatment Plans (RTPs) will be documented that summarize the: risk statement, the potential impact to GitLab, the type of risk (strategic, operational, financial or compliance), the assigned owner that will ensure the risk treatment is accomplished and a due date for the completion of the RTP. RTP status will be shared with the Audit Committee on a regular basis.


## Outputs of the ERM Program 

The outputs of the ERM program are as follows:

- Risk Appetite<br>
- Risk Assessment Criteria Matrix<br>
- Updated Company-wide Risk Register<br>
- Heat Map of top Risks<br>
- Management Action Plans for Risk Treatment

## Transparency and the ERM Program

As per GitLab's [Communication Page](https://about.gitlab.com/handbook/communication/#not-public), information about risks tracked in GitLab's Risk Register defaults to not public and limited access. Given the nature of risk management, GitLab will always be susceptible to risks. The goal of implementing risk treatment plans and carrying out risk remediation activities is to reduce the likelihood or impact (or both) of a risk occurring. Given that no risks identified can ever be fully eliminated, but instead are mitigated through reduction of likelihood and/or impact, risks that have been escalated to GitLab's Risk Register will be shared on a need-to-know basis. GitLab’s enterprise risk register will be maintained and monitored by Security Compliance and Internal Audit function.


